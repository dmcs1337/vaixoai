# Boring day write-up
Yup another python challenge but to be honest this beat me up many times which i always forgot `how to group substring in a string or a list`.
```  
>>> for i in range(0,len(y)):  
y[i] = y[i][::-1]  
>>> "".join(y)  
'aSullsbaW{4H_7HSu0D11_D__01WH7M__yC_D0}3'  
```
## Code + Explanation
As we can see, by grouping 2 character each time and reverse them u'll get the complete flag. By automatically, i'll make a script to do that.
```python
#! /usr/bin/env python  
  
enc = list('aSullsbaW{4H_7HSu0D11_D__01WH7M__yC_D0}3')  
# Saluslab{abc}  
flag = ''  
for i in xrange(0, len(enc), 2):  
  flag += ''.join(enc[i:i + 2][::-1])  
print flag
```
By jumping 2 elements in the list, we can take out (group) 2 characters by `i:i+2` and reverse them using `[::-1]` and keep appending to `flag` variable.

![enter image description here](https://i.imgur.com/lfQ7m3g.png =1000x)

Flag: `Saluslab{WH47_SH0u1D_1_D0_W17H_My__C0D3}`